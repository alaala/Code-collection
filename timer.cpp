#include <assert.h>
#include   <iostream>
#include   <stdio.h>
#include   <windows.h>
using namespace std;
 
DWORD threadID = 0;
 
DWORD WINAPI MyTimer(void* ptr)
{
	threadID = GetCurrentThreadId();
	UINT   uTimerID   =   1;
	MSG msg;
 
	uTimerID = SetTimer(NULL,   uTimerID,   5000,   NULL);
	while (GetMessage(&msg, (HWND)NULL, 0, 0))
	{
		TranslateMessage(&msg);
		switch (msg.message) {
		case WM_TIMER:
			{
				printf("WM_TIMER(0x%x) comes. TimerID = %d.\n", msg.message, msg.wParam);
				break;
			}
		default:
			{
				printf("0x%x comes.\n", msg.message);
				break;
			}
		}
	}
	printf("jump out of while loop.\n"); http://
	assert(msg.message==WM_QUIT);
	KillTimer(NULL,   uTimerID);
	return   0;
}
 
int main (int argc, char** argv)
{
	HANDLE threadHandle = CreateThread(NULL, 0, MyTimer, NULL, 0, NULL);
	while (true) {
		Sleep(20000);
		PostThreadMessage(threadID, WM_QUIT, NULL, NULL);
		break;
	}
	WaitForSingleObject(threadHandle, INFINITE);
	system("PAUSE");
	return 0;
}